/* Create Profile Summary - Javascript Function */
function createProfileSummary(){

    let userInfo = {
        firstName: document.querySelector('#firstName').value,
        lastName: document.querySelector('#lastName').value,
        age: document.querySelector('#age').value,
        currentJob: document.querySelector('#currentJob').value,
        reasonForCareerShift: document.querySelector('#reasonForCareerShift').value,
        futureGoals: document.querySelector('#futureGoals').value
    }

    if(userInfo.firstName.length === 0 || userInfo.lastName.length === 0 || userInfo.age.length === 0 || userInfo.currentJob.length === 0 || userInfo.reasonForCareerShift.length === 0 || userInfo.futureGoals.length === 0){
        alert(`*Please complete the form.`);
        return;
    }

    let summaryContainer = document.createElement(`div`);

    let summary1stParagraph = document.createElement(`p`);
    summary1stParagraph.innerText = `Hi! I'm ${userInfo.firstName} ${userInfo.lastName}.`;
    let summary2ndParagraph = document.createElement(`p`);
    summary2ndParagraph.innerText = `I am ${userInfo.age} years old.`;
    let summary3rdParagraph = document.createElement(`p`);
    summary3rdParagraph.innerText = `${userInfo.currentJob}`;
    let summary4thParagraph = document.createElement(`p`);
    summary4thParagraph.innerText = `${userInfo.reasonForCareerShift}`;
    let summary5thParagraph = document.createElement(`p`);
    summary5thParagraph.innerText = `${userInfo.futureGoals}`;

    document.querySelector(`.button-group`).before(summaryContainer);

    summaryContainer.appendChild(summary1stParagraph);
    summaryContainer.appendChild(summary2ndParagraph);
    summaryContainer.appendChild(summary3rdParagraph);
    summaryContainer.appendChild(summary4thParagraph);
    summaryContainer.appendChild(summary5thParagraph);

    summaryContainer.classList.add(`summaryContainer`, `w-50`, `mx-auto`,);
    
    document.querySelector(`#profileForm`).classList.add(`d-none`);
    document.querySelector(`#createAnotherSummaryBtn`).classList.remove(`d-none`);

}

function createAnotherSummary(){
    document.querySelector(`.summaryContainer`).remove();
    document.querySelector(`#profileForm`).classList.remove(`d-none`);
    document.querySelector(`#profileForm`).reset();
    document.querySelector(`#createAnotherSummaryBtn`).classList.add(`d-none`);
}



/* Subscription Function using If else statement */
let monthly = document.querySelector(`#monthlySubs`);
let yearly = document.querySelector(`#yearlySubs`);

function subscribe(){
    if(monthly.checked){
        alert(`Thank you! Your account is now upgraded to premium. You can cancel anytime. Next billing will be deducted next month.`);
    } else if(yearly.checked) {
        alert(`Thank you! Your account is now upgraded to premium. You can cancel anytime. Next billing will be deducted next year.`);
    } else {
        alert(`Please choose your plan and enjoy premium account!`)
    }
}



/* Check Plan Function using Ternary Operator */
function checkPlan(){
    monthly.checked || yearly.checked ? alert(`Enjoy! You still have premium account.`): alert(`Sorry, you don't have premium account. Subscribe or update your plan.`)
}




/* Theme color selector - using switch statement */
let root = document.querySelector(`:root`);

let blue = document.querySelector(`.theme-button-blue`);
let orange = document.querySelector(`.theme-button-orange`);
let pink = document.querySelector(`.theme-button-pink`);

$(`.theme-button-group`).click(function(){
    switch(true){
        case blue.checked: root.style.setProperty(`--primary`, `#00A8CC`);
        break;
        case orange.checked: root.style.setProperty(`--primary`, `#FFA41B`);
        break;
        case pink.checked: root.style.setProperty(`--primary`, `#FF8AAE`);
        break;
        default: root.style.setProperty(`--primary`, `black`);
    }
});



/* Create Table of Array Items using For Loop */
const batch163 = [
    {firstName: `Marc Allen`, lastName: `Nanong`},
    {firstName: `Joy`, lastName: `Pague`,},
    {firstName: `Roxanne`, lastName: `Talampas`},
    {firstName: `Dave`, lastName: `Supan`},
    {firstName: `Amiel`, lastName: `Canta`},
    {firstName: `Matthew`, lastName: `Blasco`},
    {firstName: `Chester`, lastName: `Clavio`},
    {firstName: `Angelito`, lastName: `Quiambao`},
    {firstName: `Kalvin Philip`, lastName: `Rosales`},
    {firstName: `Kristian`, lastName: `Zantua`},
    {firstName: `Craig Brian`, lastName: `Guarnino`},
    {firstName: `Michael`, lastName: `Bermal`},
    {firstName: `Kristin`, lastName: `Ramos`},
    {firstName: `Mauro III`, lastName: `Munoz`},
    {firstName: `Gerard`, lastName: `Apinado`},
    {firstName: `Prince Andwill`, lastName: `Monisit`},
    {firstName: `Mikhaella`, lastName: `Ambrosio`},
    {firstName: `Carine`, lastName: `Cruz`},
    {firstName: `Jeremy`, lastName: `Carangan`},
    {firstName: `Ian`, lastName: `Tablada`},
    {firstName: `Mark Allan`, lastName: `Gomez`},
    {firstName: `Gab`, lastName: `Fugaban`},
    {firstName: `Kevin`, lastName: `Tud`},
    {firstName: `Akkram`, lastName: `Bederi`}
]
let table = document.querySelector(`.table`);
let tbody = document.createElement(`tbody`);


for (let i = 0; i < batch163.length; i++){
    let tr =  document.createElement(`tr`);
    tr.classList.add(`table-row`)
    let th = document.createElement(`th`);
    th.innerText = batch163.indexOf(batch163[i]) + 1;
    th.setAttribute(`scope`, `row`);
    let tdFirstName = document.createElement(`td`);
    let tdLastName = document.createElement(`td`);
    tdFirstName.innerText = batch163[i].firstName;
    tdFirstName.classList.add(`_firstName`);
    tdLastName.innerText = batch163[i].lastName;
    tdLastName.classList.add(`_lastName`);
    table.appendChild(tbody);
    tbody.appendChild(tr);
    tr.appendChild(th);
    tr.appendChild(tdFirstName);
    tr.appendChild(tdLastName);
}


/* Counting Total Size of batch163 Array */
let totalStudentsDiv = document.querySelector(`.totalStudentsDiv`);
let totalStudents = document.createElement(`h3`);
totalStudents.innerText = `There are currently total of ${batch163.length} students in Batch163.`;
totalStudents.classList.add(`text-center`);
totalStudentsDiv.appendChild(totalStudents);

function addNewElementInTheBeginning(){
    let addFirstNameInBeginning = document.querySelector(`#addFirstNameInBeginning`).value;
    let addLastNameInBeginning = document.querySelector(`#addLastNameInBeginning`).value;
    let newStudent = {firstName: addFirstNameInBeginning, lastName: addLastNameInBeginning}
    batch163.unshift(newStudent);
    console.log(batch163);
}
function addNewElementInTheEnd(){
    let addFirstNameInEnd = document.querySelector(`#addFirstNameInEnd`).value;
    let addLastNameInEnd = document.querySelector(`#addLastNameInEnd`).value;
    let newStudent = {firstName: addFirstNameInEnd, lastName: addLastNameInEnd}
    batch163.push(newStudent);
    console.log(batch163);
}
function remove1elementFromStart(){
    batch163.shift();
    console.log(batch163);
}
function remove1elementFromEnd(){
    batch163.pop();
    console.log(batch163);
}

function findIndexOf(){
    let fName = document.querySelector(`#findIndexFirstName`).value;
    let lName = document.querySelector(`#findIndexLastName`).value;
    let index = batch163.findIndex(batch163 => batch163.firstName === fName, batch163.lastName === lName);
    let index1 = batch163.findIndex(batch163 => batch163.lastName === lName);
    console.log(index)
    console.log(index1);
    
    if(index >= 0 && index1 >= 0){
        let findIndexDiv = document.querySelector(`.find-index`);
        let findIndexP = document.createElement(`p`);
        findIndexP.innerHTML = `The index of ${fName} ${lName} is ${index}.`;
        findIndexDiv.appendChild(findIndexP);
    } else {
        let findIndexDiv = document.querySelector(`.find-index`);
        let findIndexP = document.createElement(`p`);
        findIndexP.innerHTML = `Please input the correct first name and last name.`;
        findIndexDiv.appendChild(findIndexP);
    }
}
